//IMU and Magnetorquer 
//Written by Sundeep Sarwana, Allan Goodman, Dominic Chan, Grace Liu
//Last Modified 13 June 2020

//Magnetorquer Code retrieved from
//Mark Yeo; mark.yeo@student.unsw.edu.au
//Last Modified 13Apr15
//ADC implementation for controlling a magnetorquer using an L298N H-Bridge
/*
 Arduino Pinout (+ SD shield: bend out SD shield A2-A3 legs & connect to GND & 5V respectively)
 GND  - MT GND
 A0-A5 - RT clock shield (line up SCL to A5)(daisychain MN on top)
 Pin2 - LED1 + resistor to gnd (mimics Pin5) (opt.)
 Pin3 - MN READY
 Pin4 - nothing (used by SD)
 Pin5 - MT ENABLE (grey)
 Pin6 - MT IN1 (yellow + green)
 Pin7 - MT IN2 (yellow + red)
 Pin8 - LED2 + resistor to gnd (green, mimics Pin6)
 Pin9 - LED3 + resistor to gnd (red, mimics Pin7)
 Pins10-13 - nothing (used by SD)
 */

/*Notes:
 - 'counter' variable resets to -1 every time serial monitor is opened
 */


#include <Arduino.h>
#include "magnetorquer.h"
#include "MPU9250.h"
#define MT_ON_TIME 100 //length of MT pulse (ms)
MPU9250 mpu;
int E1 = 6;
int M1 = 7;
int i = 0;
int j = 0;
int takeAvg[] = {0,0,0,0,0};
int prev_roll = 0;
int avg;
int prev_avg = 0;
int init_vel = 0;
int Kp = 5;
int deadZone = 0;

void setup()
{
    // Magnetorquer Init
    pinMode(MT_ENABLE, OUTPUT);
    pinMode(MT_IN1, OUTPUT);
    pinMode(MT_IN2, OUTPUT);
    pinMode(MT_ENABLE_LED, OUTPUT);
    pinMode(MT_IN1_LED, OUTPUT);
    pinMode(MT_IN2_LED, OUTPUT);

    digitalWrite(MT_ENABLE, HIGH);  //H-Bridge only works when ENABLE is HIGH
    digitalWrite(MT_ENABLE_LED, HIGH);
    digitalWrite(MT_IN1, LOW);
    digitalWrite(MT_IN1_LED, LOW);
    digitalWrite(MT_IN2, LOW);
    digitalWrite(MT_IN2_LED, LOW);

    // IMU Init
    pinMode(M1, OUTPUT);
    Serial.begin(9600);
    //115200
    Wire.begin();
    delay(2000);
    mpu.setup();
    delay(5000);

    // calibrate anytime you want to

    mpu.calibrateAccelGyro();
    mpu.calibrateMag();
    mpu.printCalibration();
}

void loop()
{
    static uint32_t prev_ms = millis();
    if ((millis() - prev_ms) > 100) // Can be changed to increase print rate
    {
        mpu.update();
        takeAvg[4] = mpu.getRoll();
        avg = 0;
        for (int k = 0; k < 5; k++){
            avg += takeAvg[k];
        }

    int x_vel = avg - prev_avg;
    Serial.print("AVG roll - rad/s (x-forward (north)) : ");
    Serial.println(x_vel);

    int output = Kp*x_vel;

    if(output>deadZone){
        MT_set(output,MT_FWD);
        //delay(MT_ON_TIME);
        //MT_stop();
    } else if (output<-deadZone) {
        MT_set(output,MT_REV);
        //delay(MT_ON_TIME);
        //MT_stop();
    }
    
    prev_avg = avg;
    moveDown(takeAvg);
    int curr_roll = mpu.getRoll();
    prev_ms = millis();
    i++;
    }
}

void moveDown(int takeAvg[5]){
    for (int i = 0; i < 4; i++){
        takeAvg[i] = takeAvg[i + 1];
    }
    return;
}

int MT_set(int magnitude, int dir) {
    int ret;
    if (magnitude >= 0 && magnitude <= MT_MAX){
        if (dir == MT_FWD){
            analogWrite(MT_IN1, magnitude);
            analogWrite(MT_IN1_LED, magnitude);
            digitalWrite(MT_IN2, LOW);
            digitalWrite(MT_IN2_LED, LOW );
            ret = 0;
        } else if (dir == MT_REV){
            analogWrite(MT_IN2, magnitude);
            analogWrite(MT_IN2_LED, magnitude);
            digitalWrite(MT_IN1, LOW);
            digitalWrite(MT_IN1_LED, LOW );
            ret = 0;
        } else {
            ret = 1;//invalid direction
        }
    } else {
        ret = 2;    //invalid magnitude
    }
    return ret;
}

void MT_stop(){
  MT_set(0, MT_FWD);
}

